import 'package:clean_arch_flutter_demo/features/lyrics/presentation/search_lyrics_ui.dart';
import 'package:clean_framework/clean_framework.dart';
import 'package:clean_framework/clean_framework_tests.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  uiTest(
    'Lyrics Search UI',
    context: ProvidersContext(),
    builder: () => SearchLyricsUI(),
    verify: (tester) async {
      expect(find.text('SEARCH LYRICS'), findsOneWidget);

      expect(find.byType(TextFormField), findsNWidgets(2));

      expect(find.byType(ElevatedButton), findsOneWidget);
    },
  );
}
