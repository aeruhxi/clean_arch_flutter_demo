import 'package:clean_arch_flutter_demo/entity_state.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_outputs.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_usecase.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/lyrics_providers.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/presentation/lyrics_detail_presenter.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/presentation/lyrics_detail_ui.dart';
import 'package:clean_framework/clean_framework.dart';
import 'package:clean_framework/clean_framework_providers.dart';
import 'package:clean_framework/clean_framework_tests.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

void main() {
  uiTest(
    'Lyrics Detail UI test when a lyrics is displayed',
    context: ProvidersContext(),
    builder: () => LyricsDetailUI(
      artist: "Soen",
      song: "Lotus",
      create: (builder) => FakeLyricsDetailPresenter(
          builder: builder,
          fakeLyrics: const ResourceState.data("fake lyrics")),
    ),
    verify: (tester) async {
      expect(find.text('Lyrics for Soen - Lotus'), findsOneWidget);

      expect(find.text('fake lyrics'), findsOneWidget);
    },
  );

  // Not using uiTest because uiTest uses pumpAndSettle which throws a timeout
  // for inifnitely animating widget like CircularProgressIndicator
  testWidgets("Lyrics Detail UI test when a lyrics is being fetched",
      (tester) async {
    await ProviderTester().pumpWidget(
        tester,
        MaterialApp(
          home: LyricsDetailUI(
            artist: "Soen",
            song: "Lotus",
            create: (builder) => FakeLyricsDetailPresenter(
                builder: builder, fakeLyrics: const ResourceState.loading()),
          ),
        ));
    await tester.pump(const Duration(milliseconds: 10));

    expect(find.text('Lyrics for Soen - Lotus'), findsOneWidget);

    expect(find.byType(CircularProgressIndicator), findsOneWidget);
  });

  uiTest(
    'Lyrics Detail UI test when a lyrics is not found',
    context: ProvidersContext(),
    builder: () => LyricsDetailUI(
      artist: "Soen",
      song: "Not Lotus",
      create: (builder) => FakeLyricsDetailPresenter(
          builder: builder,
          fakeLyrics: const ResourceState.error("Lyrics not found")),
    ),
    verify: (tester) async {
      expect(find.text('Lyrics for Soen - Not Lotus'), findsOneWidget);

      expect(find.text('Lyrics not found'), findsOneWidget);
    },
  );
}

class FakeLyricsDetailPresenter
    extends Presenter<LyricsDetailViewModel, LyricsDetailUIOutput, UseCase> {
  FakeLyricsDetailPresenter({
    required this.fakeLyrics,
    required PresenterBuilder<LyricsDetailViewModel> builder,
  }) : super(
          builder: builder,
          provider: lyricsUseCaseProvider,
        );

  final ResourceState<String> fakeLyrics;

  @override
  LyricsDetailViewModel createViewModel(
      UseCase useCase, LyricsDetailUIOutput output) {
    return LyricsDetailViewModel(lyrics: fakeLyrics);
  }
}
