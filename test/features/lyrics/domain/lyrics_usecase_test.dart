import 'package:clean_arch_flutter_demo/entity_state.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_inputs.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_outputs.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_usecase.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/lyrics_providers.dart';
import 'package:clean_arch_flutter_demo/providers.dart';
import 'package:clean_framework/clean_framework.dart';
import 'package:clean_framework/clean_framework_providers.dart';
import 'package:clean_framework/clean_framework_tests.dart';

import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Lyrics Usecase fetch lyrics correctly', () async {
    final usecase = LyricsUseCase();

    // mock gateway
    usecase.subscribe(
        LyricsGatewayOutput,
        (_) => Right<FailureInput, LyricsSuccessInput>(
            LyricsSuccessInput(lyrics: "this is a lyrics")));

    // Before fetching
    final initialOutput = usecase.getOutput<LyricsDetailUIOutput>();
    expect(initialOutput,
        LyricsDetailUIOutput(lyrics: const ResourceState<Never>.initial()));

    // While fetching
    final fetchFuture = usecase.fetchLyrics("Opeth", "Ghost of Perdition");
    final loadingOutput = usecase.getOutput<LyricsDetailUIOutput>();
    expect(
      loadingOutput,
      LyricsDetailUIOutput(
        lyrics: const ResourceState<Never>.loading(),
      ),
    );

    // After fetching
    await fetchFuture;
    final successOutput = usecase.getOutput<LyricsDetailUIOutput>();
    expect(
      successOutput,
      LyricsDetailUIOutput(
        lyrics: const ResourceState.data("this is a lyrics"),
      ),
    );
  });

  test('Lyrics Usecase fetch lyrics that is not found', () async {
    final usecase = LyricsUseCase();

    // mock gateway
    usecase.subscribe(
        LyricsGatewayOutput,
        (_) => Left<FailureInput, LyricsSuccessInput>(
            FailureInput(message: "No lyrics found")));

    // Before fetching
    final initialOutput = usecase.getOutput<LyricsDetailUIOutput>();
    expect(initialOutput,
        LyricsDetailUIOutput(lyrics: const ResourceState<Never>.initial()));

    // While fetching
    final fetchFuture =
        usecase.fetchLyrics("Opeth", "Song that does not exist");
    final loadingOutput = usecase.getOutput<LyricsDetailUIOutput>();
    expect(
      loadingOutput,
      LyricsDetailUIOutput(
        lyrics: const ResourceState<Never>.loading(),
      ),
    );

    // After fetching
    await fetchFuture;
    final successOutput = usecase.getOutput<LyricsDetailUIOutput>();
    expect(
      successOutput,
      LyricsDetailUIOutput(
        lyrics: const ResourceState.error("No lyrics found"),
      ),
    );
  });
}
