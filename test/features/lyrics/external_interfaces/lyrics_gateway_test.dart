import 'package:clean_arch_flutter_demo/entity_state.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_entity.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/lyrics_providers.dart';
import 'package:clean_arch_flutter_demo/providers.dart';
import 'package:clean_framework/clean_framework.dart';
import 'package:clean_framework/clean_framework_defaults.dart';
import 'package:clean_framework/clean_framework_providers.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  setUp(() {
    resetProvidersContext();
  });

  test('fetches lyrics correctly', () async {
    final useCase = lyricsUseCaseProvider.getUseCaseFromContext(
      providersContext,
    );
    final gateway = lyricsGatewayProvider.getGateway(providersContext);

    gateway.transport = (request) async {
      return Right(RestSuccessResponse(data: {"lyrics": 'test lyrics'}));
    };
    await useCase.fetchLyrics('test artist', 'test song');
    expect(
        useCase.entity,
        LyricsEntity(
            lyrics: ResourceState.data(Lyrics(content: 'test lyrics'))));
  });

  test('fetches lyrics that are not found', () async {
    final useCase = lyricsUseCaseProvider.getUseCaseFromContext(
      providersContext,
    );
    final gateway = lyricsGatewayProvider.getGateway(providersContext);

    gateway.transport = (request) async {
      return const Left(FailureResponse());
    };
    await useCase.fetchLyrics('test artist', 'test song');

    expect(useCase.entity,
        LyricsEntity(lyrics: const ResourceState.error("Lyrics not found")));
  });
}
