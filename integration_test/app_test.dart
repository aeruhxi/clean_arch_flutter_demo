import 'package:clean_arch_flutter_demo/features/lyrics/presentation/lyrics_detail_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'package:clean_arch_flutter_demo/main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('end-to-end test', () {
    testWidgets(
        'Fill in artist and song and click on button (lyrics found case)',
        (WidgetTester tester) async {
      app.main();
      await tester.pumpAndSettle();

      final artistField = find.byKey(ValueKey('ArtistTextField'));
      await tester.tap(artistField);
      await tester.pumpAndSettle();
      await tester.enterText(artistField, 'Opeth');

      final songField = find.byKey(ValueKey('SongTextField'));
      await tester.tap(songField);
      await tester.pumpAndSettle();
      await tester.enterText(songField, 'Benighted');

      final button = find.byKey(ValueKey('FindButton'));
      await tester.tap(button);
      await tester.pumpAndSettle();

      expect(find.byType(LyricsDetailUI), findsOneWidget);
      expect(find.text('Lyrics for Opeth - Benighted'), findsOneWidget);
    });
  });
}
