import 'package:clean_arch_flutter_demo/features/lyrics/lyrics_providers.dart';
import 'package:clean_framework/clean_framework.dart';
import 'package:clean_framework/clean_framework_defaults.dart';
import 'package:clean_framework/clean_framework_providers.dart';
import 'package:flutter/foundation.dart';

ProvidersContext _providersContext = ProvidersContext();

ProvidersContext get providersContext => _providersContext;

@visibleForTesting
void resetProvidersContext([ProvidersContext? context]) {
  _providersContext = context ?? ProvidersContext();
}

final restExternalInterface = ExternalInterfaceProvider(
  (_) => RestExternalInterface(
    baseUrl: 'https://api.lyrics.ovh',
    gatewayConnections: [
      () => lyricsGatewayProvider.getGateway(providersContext),
    ],
  ),
);

void loadProviders() {
  lyricsUseCaseProvider.getUseCaseFromContext(providersContext);
  restExternalInterface.getExternalInterface(providersContext);
}
