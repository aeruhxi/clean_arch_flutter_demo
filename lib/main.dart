import 'package:clean_arch_flutter_demo/providers.dart';
import 'package:clean_arch_flutter_demo/routes.dart';
import 'package:clean_framework/clean_framework.dart';
import 'package:flutter/material.dart';

void main() {
  loadProviders();
  runApp(AppRoot());
}

class AppRoot extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppProvidersContainer(
      providersContext: providersContext,
      onBuild: (context, _) {
        // providersContext().read(featureStatesProvider.featuresMap).load({
        //   'features': [
        //     {'name': 'last_login', 'state': 'ACTIVE'},
        //   ]
        // });
      },
      child: MaterialApp.router(
        routeInformationParser: router.informationParser,
        routerDelegate: router.delegate,
        theme: ThemeData(
          pageTransitionsTheme: const PageTransitionsTheme(
            builders: {
              TargetPlatform.android: ZoomPageTransitionsBuilder(),
            },
          ),
        ),
      ),
    );
  }
}
