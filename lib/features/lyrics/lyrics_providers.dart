import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_entity.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_outputs.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_usecase.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/external_interfaces/lyrics_gateway.dart';
import 'package:clean_framework/clean_framework_providers.dart';

final lyricsUseCaseProvider = UseCaseProvider<LyricsEntity, LyricsUseCase>(
  (_) => LyricsUseCase(),
);

final lyricsGatewayProvider = GatewayProvider<LyricsGateway>(
  (_) => LyricsGateway(),
);
