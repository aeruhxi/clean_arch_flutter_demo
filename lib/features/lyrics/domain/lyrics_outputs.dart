import 'package:clean_arch_flutter_demo/entity_state.dart';
import 'package:clean_framework/clean_framework_providers.dart';

class LyricsDetailUIOutput extends Output {
  LyricsDetailUIOutput({
    required this.lyrics,
  });

  final ResourceState<String> lyrics;

  @override
  List<Object?> get props => [lyrics];
}

class SearchLyricsUIOutput extends Output {
  SearchLyricsUIOutput();

  @override
  List<Object?> get props => [];
}

class LyricsGatewayOutput extends Output {
  LyricsGatewayOutput({
    required this.artist,
    required this.song,
  });

  final String artist;
  final String song;

  @override
  List<Object?> get props => [artist, song];
}
