import 'package:clean_arch_flutter_demo/entity_state.dart';
import 'package:clean_framework/clean_framework_providers.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

// ignore: unused_import
import 'package:flutter/foundation.dart';

part 'lyrics_entity.freezed.dart';

class LyricsEntity extends Entity {
  final ResourceState<Lyrics> lyrics;

  LyricsEntity({this.lyrics = const ResourceState.initial()});

  @override
  List<Object?> get props => [lyrics];

  LyricsEntity merge({
    ResourceState<Lyrics>? lyrics,
  }) {
    return LyricsEntity(
      lyrics: lyrics ?? this.lyrics,
    );
  }
}

@freezed
class Lyrics with _$Lyrics {
  factory Lyrics({required String content}) = _Lyrics;
}
