import 'package:clean_framework/clean_framework_providers.dart';

class LyricsSuccessInput extends SuccessInput {
  final String lyrics;

  LyricsSuccessInput({
    required this.lyrics,
  });

  factory LyricsSuccessInput.fromJson(Map<String, dynamic> json) {
    return LyricsSuccessInput(
      lyrics: json['lyrics'],
    );
  }
}
