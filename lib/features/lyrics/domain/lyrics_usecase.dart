import 'package:clean_arch_flutter_demo/entity_state.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_entity.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_inputs.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_outputs.dart';
import 'package:clean_framework/clean_framework_providers.dart';

class LyricsUseCase extends UseCase<LyricsEntity> {
  LyricsUseCase()
      : super(
          entity: LyricsEntity(),
          outputFilters: {
            SearchLyricsUIOutput: (LyricsEntity e) {
              return SearchLyricsUIOutput();
            },
            LyricsDetailUIOutput: (LyricsEntity e) {
              return LyricsDetailUIOutput(
                  lyrics: e.lyrics.mapData((data) => data.content));
            },
          },
        );

  Future<void> fetchLyrics(String artist, String song) async {
    entity = entity.merge(lyrics: const ResourceState.loading());

    return request<LyricsGatewayOutput, LyricsSuccessInput>(
      LyricsGatewayOutput(artist: artist, song: song),
      onSuccess: (successInput) => entity.merge(
          lyrics: ResourceState.data(Lyrics(content: successInput.lyrics))),
      onFailure: (failure) =>
          entity.merge(lyrics: ResourceState.error(failure.message)),
    );
  }
}
