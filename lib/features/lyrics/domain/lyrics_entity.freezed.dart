// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'lyrics_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$LyricsTearOff {
  const _$LyricsTearOff();

  _Lyrics call({required String content}) {
    return _Lyrics(
      content: content,
    );
  }
}

/// @nodoc
const $Lyrics = _$LyricsTearOff();

/// @nodoc
mixin _$Lyrics {
  String get content => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LyricsCopyWith<Lyrics> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LyricsCopyWith<$Res> {
  factory $LyricsCopyWith(Lyrics value, $Res Function(Lyrics) then) =
      _$LyricsCopyWithImpl<$Res>;
  $Res call({String content});
}

/// @nodoc
class _$LyricsCopyWithImpl<$Res> implements $LyricsCopyWith<$Res> {
  _$LyricsCopyWithImpl(this._value, this._then);

  final Lyrics _value;
  // ignore: unused_field
  final $Res Function(Lyrics) _then;

  @override
  $Res call({
    Object? content = freezed,
  }) {
    return _then(_value.copyWith(
      content: content == freezed
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$LyricsCopyWith<$Res> implements $LyricsCopyWith<$Res> {
  factory _$LyricsCopyWith(_Lyrics value, $Res Function(_Lyrics) then) =
      __$LyricsCopyWithImpl<$Res>;
  @override
  $Res call({String content});
}

/// @nodoc
class __$LyricsCopyWithImpl<$Res> extends _$LyricsCopyWithImpl<$Res>
    implements _$LyricsCopyWith<$Res> {
  __$LyricsCopyWithImpl(_Lyrics _value, $Res Function(_Lyrics) _then)
      : super(_value, (v) => _then(v as _Lyrics));

  @override
  _Lyrics get _value => super._value as _Lyrics;

  @override
  $Res call({
    Object? content = freezed,
  }) {
    return _then(_Lyrics(
      content: content == freezed
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Lyrics with DiagnosticableTreeMixin implements _Lyrics {
  _$_Lyrics({required this.content});

  @override
  final String content;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Lyrics(content: $content)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Lyrics'))
      ..add(DiagnosticsProperty('content', content));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Lyrics &&
            (identical(other.content, content) || other.content == content));
  }

  @override
  int get hashCode => Object.hash(runtimeType, content);

  @JsonKey(ignore: true)
  @override
  _$LyricsCopyWith<_Lyrics> get copyWith =>
      __$LyricsCopyWithImpl<_Lyrics>(this, _$identity);
}

abstract class _Lyrics implements Lyrics {
  factory _Lyrics({required String content}) = _$_Lyrics;

  @override
  String get content;
  @override
  @JsonKey(ignore: true)
  _$LyricsCopyWith<_Lyrics> get copyWith => throw _privateConstructorUsedError;
}
