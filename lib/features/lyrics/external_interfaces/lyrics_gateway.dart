import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_inputs.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_outputs.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/lyrics_providers.dart';
import 'package:clean_arch_flutter_demo/providers.dart';
import 'package:clean_framework/clean_framework_defaults.dart';
import 'package:clean_framework/clean_framework_providers.dart';

class LyricsGateway extends RestGateway<LyricsGatewayOutput, LyricsRequest,
    LyricsSuccessInput> {
  LyricsGateway()
      : super(
          context: providersContext,
          provider: lyricsUseCaseProvider,
        );

  @override
  LyricsRequest buildRequest(LyricsGatewayOutput output) {
    return LyricsRequest(artist: output.artist, song: output.song);
  }

  @override
  onSuccess(covariant RestSuccessResponse response) {
    return LyricsSuccessInput.fromJson(response.data);
  }

  @override
  FailureInput onFailure(FailureResponse failureResponse) {
    return FailureInput(message: 'Lyrics not found');
  }
}

class LyricsRequest extends GetRestRequest {
  LyricsRequest({required this.artist, required this.song});

  final String artist;
  final String song;

  @override
  String get path => '/v1/$artist/$song';

  @override
  List<Object?> get props => [artist, song];
}
