import 'package:clean_arch_flutter_demo/features/lyrics/presentation/search_lyrics_presenter.dart';
import 'package:clean_arch_flutter_demo/routes.dart';
import 'package:clean_framework/clean_framework_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class SearchLyricsUI extends UI<SearchLyricsViewModel> {
  @override
  Widget build(BuildContext context, SearchLyricsViewModel viewModel) {
    return Scaffold(
      body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: _LyricsSearchForm(onSubmit: (artist, song) {
            router.to(Routes.lyricsDetail, queryParams: {
              "artist": artist,
              "song": song,
            });
          })),
    );
  }

  @override
  Presenter<ViewModel, Output, UseCase<Entity>> create(
      PresenterBuilder<SearchLyricsViewModel> builder) {
    return SearchLyricsPresenter(builder: builder);
  }
}

class _LyricsSearchForm extends StatefulWidget {
  _LyricsSearchForm({Key? key, required this.onSubmit}) : super(key: key);

  final void Function(String artist, String song) onSubmit;

  @override
  __LyricsSearchFormState createState() => __LyricsSearchFormState();
}

class __LyricsSearchFormState extends State<_LyricsSearchForm> {
  final _formKey = GlobalKey<FormState>();

  final _artistController = TextEditingController();
  final _songController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Center(
            child: Text(
              'SEARCH LYRICS',
              style: TextStyle(fontSize: 24),
            ),
          ),
          TextFormField(
            key: Key('ArtistTextField'),
            controller: _artistController,
            decoration: const InputDecoration(
                labelText: 'Artist', hintText: "Enter an artist name"),
            validator: (String? value) {
              return (value == null || value.isEmpty)
                  ? 'Artist must not be empty.'
                  : null;
            },
          ),
          TextFormField(
            key: Key('SongTextField'),
            controller: _songController,
            decoration: const InputDecoration(
                labelText: 'Song', hintText: "Enter a song's name"),
            validator: (String? value) {
              return (value == null || value.isEmpty)
                  ? 'Song must not be empty.'
                  : null;
            },
          ),
          ElevatedButton(
            key: Key('FindButton'),
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                widget.onSubmit(_artistController.text, _songController.text);
              }
            },
            child: const Text('Find lyrics'),
          ),
        ],
      ),
    );
  }
}
