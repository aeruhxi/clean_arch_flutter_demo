import 'package:clean_arch_flutter_demo/features/lyrics/presentation/lyrics_detail_presenter.dart';
import 'package:clean_framework/clean_framework_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class LyricsDetailUI extends UI<LyricsDetailViewModel> {
  LyricsDetailUI(
      {Key? key,
      required this.artist,
      required this.song,
      PresenterCreator<LyricsDetailViewModel>? create})
      : super(key: key, create: create);

  final String artist;
  final String song;

  @override
  Widget build(BuildContext context, LyricsDetailViewModel viewModel) {
    return Scaffold(
      body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 40),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Center(
                  child: Text(
                    "Lyrics for ${artist} - ${song}",
                    style: TextStyle(fontSize: 24),
                  ),
                ),
                _buildLyrics(viewModel)
              ],
            ),
          )),
    );
  }

  Widget _buildLyrics(LyricsDetailViewModel viewModel) {
    return viewModel.lyrics.map(
      initial: (_) => Container(),
      error: (error) => Text(error.message ?? ''),
      loading: (_) => const CircularProgressIndicator(),
      data: (data) => _Lyrics(
        content: data.value,
      ),
    );
  }

  @override
  Presenter<ViewModel, Output, UseCase<Entity>> create(
      PresenterBuilder<LyricsDetailViewModel> builder) {
    return LyricsDetailPresenter(builder: builder, artist: artist, song: song);
  }
}

class _Lyrics extends StatelessWidget {
  const _Lyrics({
    Key? key,
    required this.content,
  }) : super(key: key);

  final String content;

  @override
  Widget build(BuildContext context) {
    return Text(content);
  }
}
