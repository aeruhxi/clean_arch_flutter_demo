import 'package:clean_arch_flutter_demo/entity_state.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_outputs.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_usecase.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/lyrics_providers.dart';
import 'package:clean_framework/clean_framework_providers.dart';
import 'package:flutter/cupertino.dart';

class LyricsDetailPresenter extends Presenter<LyricsDetailViewModel,
    LyricsDetailUIOutput, LyricsUseCase> {
  LyricsDetailPresenter({
    required PresenterBuilder<LyricsDetailViewModel> builder,
    required this.artist,
    required this.song,
  }) : super(
          builder: builder,
          provider: lyricsUseCaseProvider,
        );

  final String artist;
  final String song;

  @override
  void onLayoutReady(BuildContext context, LyricsUseCase useCase) {
    useCase.fetchLyrics(artist, song);
  }

  @override
  LyricsDetailViewModel createViewModel(
      LyricsUseCase useCase, LyricsDetailUIOutput output) {
    return LyricsDetailViewModel(lyrics: output.lyrics);
  }
}

class LyricsDetailViewModel extends ViewModel {
  LyricsDetailViewModel({required this.lyrics});

  final ResourceState<String> lyrics;

  @override
  List<Object?> get props => [lyrics];
}
