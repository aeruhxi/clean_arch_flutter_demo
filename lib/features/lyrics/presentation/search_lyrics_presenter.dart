import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_outputs.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/domain/lyrics_usecase.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/lyrics_providers.dart';
import 'package:clean_framework/clean_framework_providers.dart';

class SearchLyricsPresenter extends Presenter<SearchLyricsViewModel,
    SearchLyricsUIOutput, LyricsUseCase> {
  SearchLyricsPresenter(
      {required PresenterBuilder<SearchLyricsViewModel> builder})
      : super(
          builder: builder,
          provider: lyricsUseCaseProvider,
        );

  @override
  SearchLyricsViewModel createViewModel(
      LyricsUseCase useCase, SearchLyricsUIOutput output) {
    return SearchLyricsViewModel();
  }
}

class SearchLyricsViewModel extends ViewModel {
  SearchLyricsViewModel();

  @override
  List<Object?> get props => [];
}
