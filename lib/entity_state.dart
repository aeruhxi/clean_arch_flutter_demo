import 'package:freezed_annotation/freezed_annotation.dart';
// ignore: unused_import
import 'package:flutter/foundation.dart';

part 'entity_state.freezed.dart';

@freezed
class ResourceState<T> with _$ResourceState<T> {
  const factory ResourceState.data(T value) = Data;
  const factory ResourceState.loading() = Loading;
  const factory ResourceState.initial() = Initial;
  const factory ResourceState.error([String? message]) = ErrorDetails;
}

extension ResourceStateMapper<T> on ResourceState {
  ResourceState<U> mapData<U>(U Function(T data) mapper) {
    return when(
      data: (value) => ResourceState.data(mapper(value)),
      error: (value) => ResourceState.error(value),
      initial: () => const ResourceState.initial(),
      loading: () => const ResourceState.loading(),
    );
  }
}
