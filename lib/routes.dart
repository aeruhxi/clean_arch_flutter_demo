import 'package:clean_arch_flutter_demo/features/lyrics/presentation/lyrics_detail_ui.dart';
import 'package:clean_arch_flutter_demo/features/lyrics/presentation/search_lyrics_ui.dart';
import 'package:clean_framework/clean_framework.dart';
import 'package:flutter/material.dart';

enum Routes { searchLyrics, lyricsDetail }

final router = AppRouter<Routes>(
  routes: [
    AppRoute(
      name: Routes.searchLyrics,
      path: '/',
      builder: (context, state) => SearchLyricsUI(),
      routes: [
        AppRoute(
          name: Routes.lyricsDetail,
          path: 'lyrics',
          builder: (context, state) => LyricsDetailUI(
              artist: state.queryParams['artist']!,
              song: state.queryParams['song']!),
        ),
      ],
    ),
  ],
  errorBuilder: (context, state) => const NotFoundUI(),
);

class NotFoundUI extends StatelessWidget {
  const NotFoundUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text('404 Not found. This page does not exist.'),
      ),
    );
  }
}
